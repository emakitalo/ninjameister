# Ninjameister

Puzzle/platformer made with ThreeJs, Pixi, Parcel and Express. In very early stage. 

## Installation 
```
git clone  https://gitlab.com/emakitalo/ninjameister.git
cd ninjameister  
npm install  
npm run dev  
```

## Controls
Gameplay
  - Move = ArrowKeys  
  - Jump = Space  
  - Brag = b  
  - New level = r  
  - Menu = Escape  
  - Orientation = f

Menu  
  - Move = ArrowUp/ArrowDown
  - Execute = Enter  

## Bugs  
  - Collision detection is failing in some situations, it propably will be replaced with [Oimo](https://github.com/lo-th/Oimo.js)
