import { Touch, TouchClone } from "./touch";

class Input {
  private _touch: Touch;
  get touch(): Touch {
    return this._touch;
  }

  private _inputs: Array<string>;
  get inputs(): Array<string> {
    return this._inputs;
  }

  constructor() {
    const ctx: HTMLElement = document.querySelector("body");
    this._inputs = new Array();
    this._touch = new Touch(ctx);

    ctx.addEventListener(
      "keydown",
      e => {
        if (!this._inputs.includes(e.key)) {
          this._inputs.push(e.key);
        }
      },
      false
    );


    ctx.addEventListener(
      "keyup",
      e => {
        if (this._inputs.includes(e.key)) {
          this._inputs.splice(this._inputs.indexOf(e.key), 1);
        }
      },
      false
    );
  }
}

export { Input };
