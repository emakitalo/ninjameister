"use strict";

import { Vector3 } from "three";

class TouchClone {
  identifier: number;
  position: Vector3;
  direction: Vector3;
  prev: TouchClone;
  constructor(touch: any, prev: TouchClone = null) {
    this.identifier = touch.identifier;
    this.position = new Vector3(touch.screenX, touch.screenY, 0.0);
    this.direction =
      prev !== null
        ? this.position.sub(prev.position)
        : new Vector3(0.0, 0.0, 0.0);
    this.prev = prev !== null ? prev : null;
  }
}

class Touch {
  private _touches: Array<TouchClone>;
  get touches(): Array<TouchClone> {
    return this._touches;
  }

  constructor(ctx: HTMLElement) {
    this._touches = new Array();
    ctx.addEventListener(
      "touchstart",
      e => {
        this.handleStart(e);
      },
      false
    );

    ctx.addEventListener(
      "touchend",
      e => {
        this.handleEnd(e);
      },
      false
    );

    ctx.addEventListener(
      "touchcancel",
      e => {
        this.handleCancel(e);
      },
      false
    );

    ctx.addEventListener(
      "touchmove",
      e => {
        this.handleMove(e);
      },
      false
    );
  }

  add_touches(e: TouchEvent) {
    const touches: TouchList = e.changedTouches;
    for (let i: number = 0; i < touches.length; i++) {
      this._touches.push(new TouchClone(touches[i]));
    }
  }

  update_touches(e: TouchEvent) {
    const _touches: TouchList = e.changedTouches;
    for (let i = 0; i < _touches.length; i++) {
      for (let j = 0; j < this._touches.length; j++) {
        const prev = this._touches[j];
        const touch: TouchClone = new TouchClone(
          _touches[i],
          prev ? prev : null
        );
        if (touch.identifier === prev.identifier) {
          this._touches.splice(j, 1, touch);
        }
      }
    }
  }

  remove_touches(e: TouchEvent) {
    const _touches: TouchList = e.changedTouches;
    for (let i: number = 0; i < _touches.length; i++) {
      const touch = _touches[i];
      for (let j = 0; j < this._touches.length; j++) {
        const last: TouchClone = this._touches[j];
        if (touch.identifier === last.identifier) {
          this._touches.splice(i, 1);
        }
      }
    }
  }

  handleStart(e: TouchEvent) {
    this.add_touches(e);
  }

  handleEnd(e: TouchEvent) {
    this.remove_touches(e);
  }

  handleCancel(e: TouchEvent) {
    this.remove_touches(e);
  }

  handleMove(e: TouchEvent) {
    this.update_touches(e);
  }
}

export { Touch, TouchClone };
