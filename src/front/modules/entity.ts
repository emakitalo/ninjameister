import {
  Vector3,
  Quaternion,
  Euler,
  Raycaster,
  Intersection,
  Object3D,
  Sphere,
  Matrix4
} from "three";

class Entity {
  protected _id: number;
  get id(): number {
    return this._id;
  }
  set id(id: number) {
    this._id = id;
  }
  private _scenePath: string;
  protected _scene: any;
  get scene() {
    return this._scene;
  }
  set scene(scene: any) {
    this._scene = scene;
  }
  protected _position: Vector3;
  get position() {
    return this._position;
  }
  set position(position: Vector3) {
    this._position = position;
  }
  protected _lastPosition: Vector3;
  get lastPosition() {
    return this._position;
  }
  set lastPosition(lastPosition: Vector3) {
    this._lastPosition = lastPosition;
  }
  protected _velocity: Vector3;
  get velocity() {
    return this._velocity;
  }
  protected _acceleration: Vector3;
  get acceleration() {
    return this._acceleration;
  }
  protected _friction: number;
  get friction(): number {
    return this._friction;
  }
  protected _airResistance: number;
  get airResistance(): number {
    return this._airResistance;
  }
  protected _speed: number;
  get speed(): number {
    return this._speed;
  }
  protected _rSpeed: number;
  get rSpeed(): number {
    return this._rSpeed;
  }
  protected _target: Object3D;
  get target() {
    return this._target;
  }
  protected _rotation: Euler;
  get rotation(): Euler {
    return this._rotation;
  }
  protected _raycast: Raycaster;
  get raycast(): Raycaster {
    return this._raycast;
  }
  protected _boundingSphere: Sphere;
  get boundingSphere() {
    return this._boundingSphere;
  }
  set boundingSphere(boundingSphere: Sphere) {
    this._boundingSphere = boundingSphere;
  }
  protected _raycastOrigin: Vector3;
  protected _active: boolean;
  get active() {
    return this._active;
  }
  set active(active: boolean) {
    this._active = active;
  }
  protected _matrix: Matrix4;
  get matrix() {
    return this._matrix;
  }
  set matrix(matrix: Matrix4) {
    this._matrix = matrix;
  }
  protected _grounded: boolean;
  get grounded() {
    return this._grounded;
  }
  set grounded(grounded: boolean) {
    this._grounded = grounded;
  }
  protected _onObject: Object3D;
  get onObject() {
    return this._onObject;
  }
  set onObject(onObject: Object3D) {
    this._onObject = onObject;
  }
  protected _lastIntersectionId: number;
  get lastIntersectionId() {
    return this._lastIntersectionId;
  }
  set lastIntersectionId(lastIntersection: number) {
    this._lastIntersectionId = lastIntersection;
  }
  protected _intersectionId: number;
  get intersectionId() {
    return this._intersectionId;
  }
  set intersectionId(intersectionId: number) {
    this._intersectionId = intersectionId;
  }
  protected _lastVelocity: Vector3;
  get lastVelocity() {
    return this._lastVelocity;
  }
  set lastVelocity(lastVelocity: Vector3) {
    this._lastVelocity = lastVelocity;
  }

  private _xUp: Vector3;
  private _yUp: Vector3;
  private _zUp: Vector3;

  constructor(id: number = 0) {
    this._id = id;
    this._raycast = new Raycaster();
    this._raycastOrigin = new Vector3(0, 0, 0);
    this._position = new Vector3(0, -1, 0);
    this._velocity = new Vector3(0, 0, 0);
    this._acceleration = new Vector3(0, 0, 0);
    this._target = new Object3D();
    this._rotation = new Euler();
    this._xUp = new Vector3(1, 0, 0);
    this._yUp = new Vector3(0, 1, 0);
    this._zUp = new Vector3(0, 0, 1);
    this._target.parent = this._scene;
    this._target.position.set(0, 0, 5);
    this._friction = 0.90;
    this._airResistance = 0.95;
    this._speed = 1;
    this._rSpeed = 3;
    this._matrix = new Matrix4();
    this._lastIntersectionId = -1;
    this._intersectionId = -1;
    this._lastVelocity = this._velocity.clone();
  }

  public applyMatrix(): void {
    this._scene.matrix.copy(this._matrix);
  }

  public getMatrix(): Matrix4 {
    this._matrix.makeRotationFromEuler(this._rotation);
    this._matrix.setPosition(this._scene.parent.localToWorld(this._position.clone()));
    return this._matrix;
  }

  public raycastForward(objects: Object3D): Array<Intersection> {
    const o: Vector3 = this._raycastOrigin.clone();
    return this.getIntersections(objects, o, this.getDirection());
  }

  public raycastDown(objects: Object3D): Array<Intersection> {
    const o: Vector3 = this._raycastOrigin.clone();
    return this.getIntersections(objects, o, this._yUp.clone().multiplyScalar(-1));
  }

  public getIntersections(
    objects: Object3D,
    origin: Vector3,
    direction: Vector3
  ): Array<Intersection> {
    this._raycast.set(origin, direction);
    return this._raycast.intersectObjects(objects.children, true);
  }

  public getDirection(): Vector3 {
    return this._scene
      .localToWorld(this._target.position.clone())
      .sub(this._scene.position)
      .normalize();
  }

  public MoveX(x: number): void {
    this._acceleration.x += x;
  }

  public MoveY(y: number): void {
    this._acceleration.y += y;
  }

  public MoveZ(z: number): void {
    this._acceleration.z += z;
  }

  public Move(xyz: Vector3): void {
    this._acceleration.add(xyz);
  }

  public UpdateMove(delta: number): void {
    this._lastPosition = this._position.clone();
    this._velocity.add(this._acceleration.multiplyScalar(delta));
    this._position.add(this._velocity);
    this._acceleration.multiplyScalar(0);
    this._lastVelocity = this._velocity.clone();
  }

  public RotateX(x: number, delta: number): void {
    this._rotation.x = (this._rotation.x + x * delta) % 360;
    this._scene.rotation.setFromVector3(this._rotation.toVector3());
  }

  public RotateY(y: number, delta: number): void {
    this._rotation.y = (this._rotation.y + y * delta) % 360;
    this._scene.rotation.setFromVector3(this._rotation.toVector3());
  }

  public RotateZ(z: number, delta: number): void {
    this._rotation.x = (this._rotation.z + z * delta) % 360;
    this._scene.rotation.setFromVector3(this._rotation.toVector3());
  }
}

export { Entity };
