import {
  LoadingManager,
  AnimationMixer,
  AnimationClip,
  LoopOnce,
  Scene,
  AnimationAction
} from "three";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { Entity } from "./entity";
import { HashTable } from "./interfaces";

class MeshLoader extends Entity {
  protected _actions: Array<AnimationClip>;
  get actions(): Array<AnimationClip> {
    return this._actions;
  }
  protected _mixer: AnimationMixer;
  get mixer() {
    return this._mixer;
  }

  constructor(
    scenePath: string,
    manager: LoadingManager,
    parent: Scene,
    defaultAction: string = "Armature|idle"
  ) {
    super();
    const loader: GLTFLoader = new GLTFLoader(manager);
    loader.load(scenePath, gltf => {
      this._scene = gltf.scene;
      this._boundingSphere = this.scene.children[0].children[0].children[1].geometry.boundingSphere;
      parent.add(this._scene);
      this._actions = gltf.animations;
      this._mixer = new AnimationMixer(this._scene);
      gltf.animations.forEach(clip => {
        const action: AnimationAction = this._mixer.clipAction(clip);
        action.clampWhenFinished = true;
        if (clip.name === defaultAction) {
          action.play();
        } else {
          action.setLoop(LoopOnce, 1);
        }
      });
      this._scene.traverse(function (child) {
        if (child.isMesh) {
          child.castShadow = true;
          child.receiveShadow = true;
        }
      });
    });
  }
}

export { MeshLoader };
