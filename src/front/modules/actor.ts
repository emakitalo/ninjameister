import {
  LoadingManager,
  Scene,
} from "three";
import { MeshLoader } from "./meshloader";

class Actor extends MeshLoader {
  private _name: string;
  constructor(
    scenePath: string,
    scene: Scene,
    manager: LoadingManager,
    name: string = "Player"
  ) {
    super(scenePath, manager, scene);
    this._name = name;
  }

  public update(delta: number): void {
    this.UpdateMove(delta);
    if(this._grounded)
      this._velocity.multiplyScalar(this._friction);
    else
      this._velocity.multiplyScalar(this._airResistance);
  }
}

export { Actor };
