import {
  Object3D,
  Clock,
  Scene,
  PerspectiveCamera,
  LoadingManager,
  WebGLRenderer,
  AnimationAction,
  AnimationClip,
  Mesh,
  BoxBufferGeometry,
  MeshPhongMaterial,
  Group,
  AmbientLight,
  PointLight,
  PCFSoftShadowMap,
  PlaneGeometry,
  Vector3,
  Intersection,
  InstancedBufferGeometry,
  InstancedMesh,
  InstancedBufferAttribute,
  BufferGeometry
} from "three";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { Input } from "./modules/input";
import { Entity } from "./modules/entity";
import { Actor } from "./modules/actor";
import { Application, Text, TextStyle, Container, Graphics } from "pixi.js";
import { HashTable } from "./modules/interfaces";
import { Touch } from "./modules/touch";
import { MeshLoader } from "./modules/meshloader";

function mod(n1: number, n2: number): number {
  return ((n1 % n2) + n2) % n2;
}

let txtstyle = new TextStyle({
  fontFamily: "Arial",
  fontSize: 36,
  fill: "white"
});

let menuItemStyle = new TextStyle({
  fontFamily: "Arial",
  fontSize: 24,
  fill: "white",
  dropShadow: false,
  dropShadowColor: 0x00ff00,
  dropShadowAlpha: 1,
  dropShadowAngle: 0,
  dropShadowBlur: 20,
  dropShadowDistance: 0
});

const manager = new LoadingManager();
const input: Input = new Input();
const clock: Clock = new Clock();
const scene: Scene = new Scene();
const renderer: WebGLRenderer = new WebGLRenderer();
document.body.appendChild(renderer.domElement);
const pixi: Application = new Application({
  width: window.innerWidth,
  height: window.innerHeight,
  transparent: true
});
const loadPercents: Text = new Text("0%");
loadPercents.position.set(window.innerWidth / 2, window.innerHeight / 2);
pixi.stage.addChild(loadPercents);

const hud: Container = new Container();
let menuActive: boolean = true;
hud.visible = false;
const scoreText: Text = new Text("0/10", txtstyle);
scoreText.position.set(50, 50);
hud.addChild(scoreText);
const levelText: Text = new Text("", txtstyle);
levelText.position.set(window.innerWidth - 150, 50);
hud.addChild(levelText);

const menu: Container = new Container();
const title: Text = new Text("NINJAMEISTER", txtstyle);
const start: Text = new Text("Start", menuItemStyle);
const menuBg: Graphics = new Graphics();
menuBg.beginFill(0x666666);
menuBg.alpha = 0.25;
menuBg.drawRect(0, 0, window.innerWidth, window.innerHeight);
menuBg.endFill();
menu.addChild(menuBg);
title.position.set(
  window.innerWidth / 2 - title.width / 2,
  window.innerHeight / 2 - title.height / 2
);
menu.addChild(title);
start.position.set(
  window.innerWidth / 2 - start.width / 2,
  window.innerHeight / 2 - start.height / 2
);
menu.addChild(start);
menu.visible = false;
const menuItems: Array<Text> = [start];
let activeItem: number = 0;

pixi.stage.addChild(hud);
pixi.stage.addChild(menu);
document.body.appendChild(pixi.view);

const cameraPivot: Group = new Group();
const camera: PerspectiveCamera = new PerspectiveCamera(
  75,
  window.innerWidth / window.innerHeight,
  0.1,
  1000
);
const camY = 2;
const camZ = 5;

let delta: number = clock.getDelta();
let locked: boolean = false;

const pivot: Group = new Group();
const world: Group = new Group();
const gravity: Vector3 = new Vector3(0, 0.988, 0);
let gravityOn: boolean = false;
const player: Actor = new Actor(
  "assets/models/cibus_ninja_2.glb",
  scene,
  manager
);

const playerControl: HashTable<Function> = {
  " ": (obj: Actor): void => {
    if (obj.grounded) {
      obj.grounded = false;
      obj.MoveY(-40);
    }
  },
  b: (obj: Actor): void => {
    playAction(obj, "Armature|brag");
  },
  c: (obj: Actor): void => {
    playAction(obj, "Armature|brag");
  },
  ArrowUp: (obj: Actor): void => {
    const d: Vector3 = obj.getDirection().multiplyScalar(obj.speed * -1);
    d.y = 0;
    obj.Move(d);
  },
  ArrowDown: (obj: Actor): void => {
    const d: Vector3 = obj.getDirection().multiplyScalar(obj.speed);
    d.y = 0;
    obj.Move(d);
  },
  ArrowRight: (obj: Actor): void => {
    obj.RotateY(obj.rSpeed, delta);
  },
  ArrowLeft: (obj: Actor): void => {
    obj.RotateY(-obj.rSpeed, delta);
  },
  Escape: (obj: Actor): void => {
    menu.visible = true;
  },
  r: (obj: Actor): void => {
    init();
  },
  f: (obj: Actor): void => {
    cameraPivot.rotation.y = mod(-obj.rotation.y, Math.PI * 2);
  }
};

const menuControl: HashTable<Function> = {
  Enter: (): void => {
    switch (activeItem) {
      case 0:
        menu.visible = false;
        hud.visible = true;
        break;
    }
  },
  ArrowDown: (): void => {
    menuItems[activeItem].style.dropShadow = false;
    activeItem = mod(activeItem + 1, menuItems.length);
    menuItems[activeItem].style.dropShadow = true;
  },
  ArrowUp: (): void => {
    menuItems[activeItem].style.dropShadow = false;
    activeItem = mod(activeItem - 1, menuItems.length);
    menuItems[activeItem].style.dropShadow = true;
  },
  ArrowLeft: (): void => {}
};

const box: BoxBufferGeometry = new BoxBufferGeometry(2, 2, 2);
let material: MeshPhongMaterial = new MeshPhongMaterial({
  color: 0x000000
});
let enemyMesh: InstancedMesh;
let enemyBuffer: InstancedBufferGeometry;
const enemyCount: number = 10;
const enemies: Array<Entity> = new Array<Entity>();
const enemyColors: Float32Array = new Float32Array(enemyCount * 3);
const solved: Array<number> = new Array<number>();

let level = 0;
let score = 0;

const groundMaterial: MeshPhongMaterial = new MeshPhongMaterial({
  color: 0x000000
});
const groundGeo: PlaneGeometry = new PlaneGeometry(100, 100);
const ground: Entity = new Entity();

const ambient = new AmbientLight(0xffffff);
const point = new PointLight(0xffffff, 0.75, 100);

const raycastObjects: Group = new Group();
const collisionObjects: Group = new Group();

function playAction(obj: Actor, name: string): void {
  const action: AnimationAction = obj.mixer.clipAction(
    AnimationClip.findByName(obj.actions, name)
  );
  if (!action.isRunning()) {
    action.reset();
    action.enabled = true;
    action.play();
  }
}

function handleTouches(touch: Touch): void {}

function handlePlayKeys(
  obj: Actor,
  actions: HashTable<Function>,
  keys: Array<string>
): void {
  for (let i: number = 0; i < keys.length; i++) {
    if (Object.keys(actions).includes(keys[i])) actions[keys[i]](obj);
  }
}

function handleMenuKeys(
  actions: HashTable<Function>,
  keys: Array<string>
): void {
  for (let i: number = 0; i < keys.length; i++) {
    if (Object.keys(actions).includes(keys[i])) actions[keys[i]]();
  }
}

function handleInput(
  obj: Actor,
  actions: HashTable<Function>,
  input: Input
): void {
  if (input.inputs.length > 0) {
    handlePlayKeys(obj, actions, input.inputs);
  } else {
    handleTouches(input.touch);
  }
}

function resetActions(actor: Actor, action: string = "Armature|idle"): void {
  input.inputs.length = 0;
  input.touch.touches.length = 0;
  actor.mixer.stopAllAction();
  playAction(actor, action);
}

function handleRayIntersectionsForward(
  obj: Actor,
  intersections: Array<Intersection>
): void {
  if (intersections.length > 0) {
    let locked: boolean = false;
    for (let i: number = 0; i < intersections.length; i++) {
      const isec: Intersection = intersections[i];
      if (isec.distance < 1) {
        if (!locked) {
          locked = true;
        }
      }

      if (locked) {
        //resetActions(player);
      }
    }
  }
}

function handleRayIntersectionsDown(
  obj: Actor,
  intersections: Array<Intersection>
): void {
  if (intersections.length > 0) {
    const nearest = intersections[0];
    let isec: Intersection = null;
    let id: number = null;
    for (let i: number = 0; i < intersections.length; i++) {
      isec = intersections[i];
      const pos: Vector3 = pivot.localToWorld(isec.point.clone());
      const len: number = obj.velocity.length();

      if (isec.distance <= len) {
        const iObj: InstancedMesh = isec.object as InstancedMesh;
        const idx: number = isec.instanceId >= 0 ? isec.instanceId * 3 : null;
        if (idx !== null) {
          const buffer: InstancedBufferGeometry = iObj.geometry as InstancedBufferGeometry;
          const colors: InstancedBufferAttribute = buffer.attributes
            .colors as InstancedBufferAttribute;
          colors.set([0, 1, 0], idx);
          colors.needsUpdate = true;
          if (!solved.includes(idx)) {
            solved.push(idx);
            score++;
            scoreText.text = score + "/" + enemyCount;
            if (solved.length === enemyCount) {
              init();
            }
          }
        }

        obj.position = pos;
        id = isec.object.id;
        obj.onObject = isec.object;
        obj.intersectionId = isec.instanceId ? isec.instanceId : -1;
        obj.lastIntersectionId = obj.intersectionId;
        obj.velocity.multiplyScalar(0);
        obj.grounded = true;
        break;
      }
    }

    if (
      nearest.object.id === obj.onObject.id &&
      nearest.instanceId !== obj.lastIntersectionId &&
      nearest.instanceId !== undefined &&
      obj.lastIntersectionId !== -1
    ) {
      obj.grounded = false;
    } else if (isec.object.id !== obj.onObject.id && intersections.length < 2) {
      obj.grounded = false;
    }
  }

  if (!obj.grounded) {
    player.Move(gravity.clone());
  }
}

function checkCollision(v1: Vector3, v2: Vector3, d: number): boolean {
  return v1.distanceTo(v2) < d;
}

function handleSphereCollision(obj: Actor, objs: Array<Entity>): void {
  const objPos: Vector3 = obj.boundingSphere.center.clone();
  const objRadius: number = obj.boundingSphere.radius;

  for (let i: number = 0; i < objs.length; i++) {
    const obj2: Entity = objs[i];
    const obj2Pos: Vector3 = obj2.boundingSphere.center
      .clone()
      .add(pivot.localToWorld(obj2.position.clone()));
    const obj2Radius: number = obj2.boundingSphere.radius;
    const iId: number = obj.intersectionId;
    const pId: number = obj.lastIntersectionId;
    if (
      obj2.scene.id !== obj.onObject.id ||
      (obj2.scene.id === obj.onObject.id &&
        iId !== i &&
        objs[iId] ? objs[iId].position.y : obj2.position.y > obj2.position.y)
    ) {
      if (checkCollision(objPos, obj2Pos, objRadius + obj2Radius)) {
        const dir1: Vector3 = obj.lastVelocity.clone().normalize();
        const dir2: Vector3 = objPos.clone().sub(obj2Pos).normalize();
        const dir3: Vector3 = obj.getDirection();
        const dir4: Vector3 = obj.velocity.clone().normalize();
        const dist: number = objRadius + obj2Radius - objPos.distanceTo(obj2Pos) - 0.05;
        const angl1: number = dir4.dot(new Vector3(0, 1, 0));
        const angl2: number = dir4.dot(dir2);
        const angl3: number = dir4.dot(dir3);
        const yDist = obj2.position.y + obj.position.y ;
        console.log(obj.position.y, obj2.position.y, yDist);
        if (angl1 <= 1 && yDist >= 0) {
          const force: number = Math.pow(dist + 0.75, 4);
          if (angl2 <= 0) {
            dir1.y = 0;
            dir2.y = 0;
            if (Math.round(angl3) === -1) {
              obj.Move(dir2.reflect(dir2).multiplyScalar(force));
              //obj.Move(dir2.multiplyScalar(-force));
            } else {
              obj.position = obj.lastPosition;
              obj.Move(dir2.reflect(dir2).multiplyScalar(force));
            }
          } else {
            obj.position = obj.lastPosition;
            obj.Move(dir1.multiplyScalar(-force));
          }
          resetActions(obj);
          break;
        } else {
        }
      }
    }
  }
}

function animate(): void {
  delta = clock.getDelta();
  if (!menu.visible) {
    if (input.inputs.length > 0 || input.touch.touches.length > 0) {
      handleInput(player, playerControl, input);
    }

    handleSphereCollision(player, enemies);
    handleRayIntersectionsForward(
      player,
      player.raycastForward(raycastObjects)
    );
    handleRayIntersectionsDown(player, player.raycastDown(raycastObjects));

    camera.lookAt(cameraPivot.position);
    player.update(delta);
    player.mixer.update(delta);
    pivot.position.set(player.position.x, player.position.y, player.position.z);
    renderer.render(scene, camera);
  } else {
    if (input.inputs.length > 0 || input.touch.touches.length > 0) {
      handleMenuKeys(menuControl, input.inputs);
    }
  }
  pixi.renderer.render(pixi.stage);
  requestAnimationFrame(animate);
}

manager.onProgress = (url, itemsLoaded, itemsTotal): void => {
  loadPercents.text = itemsLoaded + "%";
  console.log(
    "Loading file: " +
      url +
      ".\nLoaded " +
      itemsLoaded +
      " of " +
      itemsTotal +
      " files."
  );
};

function resize(): void {
  const w: number = window.innerWidth;
  const h: number = window.innerHeight;
  camera.aspect = w / h;
  camera.updateProjectionMatrix();
  renderer.setSize(w, h);
  levelText.position.set(w - 150, 50);
  menuBg.clear();
  menuBg.beginFill(0x666666);
  menuBg.drawRect(0, 0, w, h);
  menuBg.endFill();
  loadPercents.position.set(w / 2, h / 2);
  title.position.set(w / 2 - title.width / 2, h / 2 - title.height * 2);
  start.position.set(w / 2 - start.width / 2, h / 2);
  pixi.renderer.resize(w, h);
}

function init(): void {
  player.grounded = false;
  level++;
  player.position = new Vector3(0, -1, 0);
  pivot.position.set(player.position.x, player.position.y, player.position.z);
  score = 0;
  scoreText.text = score + "/" + enemyCount;
  levelText.text = "lvl:" + level;
  solved.length = 0;
  for (let i: number = 0; i < enemyCount; i++) {
    enemyColors[i * 3] = Math.random();
    enemyColors[i * 3 + 1] = Math.random();
    enemyColors[i * 3 + 2] = Math.random();
    enemies[i] = new Entity();
    let enemy: Entity = enemies[i];
    enemy.scene = enemyMesh;
    enemy.scene.receiveShadow = true;
    enemy.scene.castShadow = true;
    enemy.boundingSphere = box.boundingSphere;
    enemy.position.set(
      Math.round(Math.round(Math.random() * 20)),
      Math.round(Math.random() * 5) + 1,
      Math.round(Math.round(Math.random() * 20))
    );
    enemyMesh.setMatrixAt(i, enemies[i].getMatrix());
    player.onObject = ground.scene;
    enemyMesh.instanceMatrix.needsUpdate = true;
  }
  enemyBuffer.setAttribute(
    "colors",
    new InstancedBufferAttribute(enemyColors, 3)
  );
}

window.onresize = (): void => {
  resize();
};

manager.onLoad = (): void => {
  loadPercents.visible = false;
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = PCFSoftShadowMap;
  ambient.intensity = 0.5;
  point.position.set(5, 5, 5);
  point.shadow.mapSize.width = 512; // default
  point.shadow.mapSize.height = 512; // default
  point.shadow.camera.near = 0.5; // default
  point.shadow.camera.far = 500; // default
  point.castShadow = true;
  point.shadow.radius = 14;
  box.computeBoundingSphere();
  box.computeBoundingBox();
  ground.scene = new Mesh(groundGeo, groundMaterial);
  ground.scene.receiveShadow = true;

  material.onBeforeCompile = shader => {
    shader.vertexShader = "" + shader.vertexShader;

    shader.vertexShader = shader.vertexShader
      .replace(
        "#define PHONG",
        [
          "#define PHONG",
          "attribute vec3 colors;",
          "varying vec3 colorOut;"
        ].join("\n")
      )
      .replace(
        "void main() {",
        ["void main() {", "colorOut = colors;"].join("\n")
      );

    shader.fragmentShader = shader.fragmentShader
      .replace(
        "#define PHONG",
        ["#define PHONG", "varying vec3 colorOut;"].join("\n")
      )
      .replace(
        "vec4 diffuseColor = vec4( diffuse, opacity );",
        ["vec4 diffuseColor = vec4(diffuse + colorOut, opacity );"].join("\n")
      );
  };

  player.boundingSphere.center.multiplyScalar(0);
  player.boundingSphere.center.y = 0.75;
  player.boundingSphere.radius = 0.75;

  enemyBuffer = new InstancedBufferGeometry();
  enemyBuffer.copy(box);
  enemyMesh = new InstancedMesh(enemyBuffer, material, enemyCount);
  enemyMesh.parent = collisionObjects;

  renderer.setSize(window.innerWidth, window.innerHeight);

  cameraPivot.add(camera);
  camera.position.set(0, camY, camZ);
  cameraPivot.position.y = 1;
  cameraPivot.rotateY(Math.PI);

  ground.scene.rotateX(-Math.PI / 2);
  raycastObjects.parent = pivot;

  raycastObjects.add(ground.scene);
  raycastObjects.add(enemyMesh);
  world.add(pivot);
  pivot.add(raycastObjects);
  pivot.add(point);
  scene.add(ambient);
  scene.add(world);
  scene.add(cameraPivot);

  menu.visible = true;
  clock.start();
  init();
  resize();
  animate();

  console.log("Loading complete!");
};
