const express = require("express");
const app = express();
const http = require("http").createServer(app);

app.use(express.static("dist"));

const template = "../dist/index.html";

app.get("/", (req, res) => {
  res.sendFile(template);
});

http.listen(8888);
